#include <utils/RefBase.h>
#include <binder/IPCThreadState.h>
#include "DemoApplication.h"

using namespace android;

int main()
{
    DemoApplication* pApp = new DemoApplication();
    pApp->run_example();

    ProcessState::self()->startThreadPool();
    IPCThreadState::self()->joinThreadPool();

    return 0;
}
