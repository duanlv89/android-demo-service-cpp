#define LOG_TAG "APP_01"
#include <utils/Log.h>
#include "DemoApplication.h"

DemoApplication::DemoApplication()
{
    sp<IServiceManager> sm = defaultServiceManager();
    if (sm != nullptr) {
        sp<IBinder> binder = sm->getService(String16(DEMO_SERVICE_NAME));
        pDemoSvc = IDemoService::asInterface(binder);

        // register a Listener
        pListener = new DemoAppListenerStub(*this);
        pDemoSvc->registerListener(pListener);
    } else {
        // @TODO: error handling
    }
}

void DemoApplication::onAddNumberRlt(int32_t result)
{
    ALOGV("onAddNumberRlt(12, 13)=%d\n", result);
}

void DemoApplication::onMaxNumberRlt(int32_t result)
{
    ALOGV("onMaxNumberRlt(16, 13)=%d\n", result);
}

void DemoApplication::run_example(void)
{
    if (pDemoSvc != nullptr) {
        // call synchronous function
        ALOGV("AddNumber(12, 13)=%d\n", pDemoSvc->addNumber(12, 13));
        ALOGV("MaxNumber(16, 13)=%d\n", pDemoSvc->maxNumber(16, 13));

        // call asynchronous function
        pDemoSvc->addNumberAsync(12, 13);
    }
}

void DemoApplication::DemoAppListenerStub::onAddNumberRlt(int32_t result)
{
    mParent.onAddNumberRlt(result);
}

void DemoApplication::DemoAppListenerStub::onMaxNumberRlt(int32_t result)
{
    mParent.onMaxNumberRlt(result);
}
