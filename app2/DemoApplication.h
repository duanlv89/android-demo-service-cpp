#if !defined(_DEMO_APPLICATION_H_)
#define _DEMO_APPLICATION_H_

#include "DemoService/IDemoListener.h"
#include "DemoService/IDemoService.h"
#include <binder/IServiceManager.h>
#include <utils/RefBase.h>

using namespace android;

class DemoApplication;

class DemoApplication {
public:
    DemoApplication();
    virtual ~DemoApplication() {}

    void run_example(void);
    void onAddNumberRlt(int32_t result);
    void onMaxNumberRlt(int32_t result);

public:
    class DemoAppListenerStub : public BnDemoListener {
    public:
        DemoAppListenerStub(DemoApplication& parent)
            : mParent(parent)
        {
        }
        virtual ~DemoAppListenerStub() {}
        virtual void onAddNumberRlt(int32_t result);
        virtual void onMaxNumberRlt(int32_t result);

    private:
        DemoApplication& mParent;

        friend class DemoApplication;
    };

private:
    sp<IDemoService> pDemoSvc;
    sp<DemoAppListenerStub> pListener;
};

#endif //! defined(_DEMO_APPLICATION_H_)
