#include <utils/RefBase.h>
#include <binder/IPCThreadState.h>
#include "DemoApplication.h"

using namespace android;

int main()
{
    DemoApplication* pApp = new DemoApplication();
    // pApp->run_example();
    // app 2 just listen to service only, do not send req

    ProcessState::self()->startThreadPool();
    IPCThreadState::self()->joinThreadPool();

    return 0;
}
