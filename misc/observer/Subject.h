///////////////////////////////////////////////////////////
//  Subject.h
//  Implementation of the Interface Subject
//  Created on:      06-Apr-2020 12:38:01 PM
//  Original author: duanlv
///////////////////////////////////////////////////////////

#if !defined(EA_A52C0A9C_23D6_42b4_B813_D6F704D6E297__INCLUDED_)
#define EA_A52C0A9C_23D6_42b4_B813_D6F704D6E297__INCLUDED_

#include "Observer.h"
#include <algorithm>
#include <iostream>
#include <stdint.h>
#include <vector>

class Subject {

public:
    virtual ~Subject() {}
    virtual void registerObserver(Observer* observer) = 0;
    virtual void unregisterObserver(Observer* observer) = 0;
    virtual void notify() = 0;
    virtual int getState() = 0;
};

#endif // !defined(EA_A52C0A9C_23D6_42b4_B813_D6F704D6E297__INCLUDED_)
