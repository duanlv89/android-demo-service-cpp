#include "ClientObserver.h"
#include "ServiceSubject.h"

enum E_CLIENT_ID {
    CLIENT_ID_NONE = 0,
    CLIENT_ID_1 = 1,
    CLIENT_ID_2 = 2
};

int main(void)
{

    ServiceSubject* service = new ServiceSubject();
    ClientObserver* pclient1 = new ClientObserver(CLIENT_ID_1);
    ClientObserver* pclient2 = new ClientObserver(CLIENT_ID_2);

    std::cout << "Client 1 state: " << pclient1->getState() << '\n';
    std::cout << "Client 2 state: " << pclient2->getState() << '\n';
    service->registerObserver(pclient1);
    service->registerObserver(pclient2);
    service->setState(10);
    service->notify();
    std::cout << "Client 1 state: " << pclient1->getState() << '\n';
    std::cout << "Client 2 state: " << pclient2->getState() << '\n';

    service->unregisterObserver(pclient1);
    service->setState(20);
    service->notify();
    std::cout << "Client 1 state: " << pclient1->getState() << '\n';
    std::cout << "Client 2 state: " << pclient2->getState() << '\n';

    service->unregisterObserver(pclient2);
    service->setState(30);
    service->notify();
    std::cout << "Client 1 state: " << pclient1->getState() << '\n';
    std::cout << "Client 2 state: " << pclient2->getState() << '\n';

    service->registerObserver(pclient1);
    service->registerObserver(pclient2);
    service->notify();
    std::cout << "Client 1 state: " << pclient1->getState() << '\n';
    std::cout << "Client 2 state: " << pclient2->getState() << '\n';

    delete service;
    delete pclient1;
    delete pclient2;

    return 0;
}
