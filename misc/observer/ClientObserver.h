///////////////////////////////////////////////////////////
//  ClientObserver.h
//  Implementation of the Class ClientObserver
//  Created on:      06-Apr-2020 12:47:53 PM
//  Original author: duanlv
///////////////////////////////////////////////////////////

#if !defined(EA_8E02386E_3A21_48f6_9655_702E0977DB5E__INCLUDED_)
#define EA_8E02386E_3A21_48f6_9655_702E0977DB5E__INCLUDED_

#include "Observer.h"

class Subject;

class ClientObserver : public Observer {
public:
    ClientObserver(uint16_t id);
    virtual ~ClientObserver();

    void onNotification(Subject* subject);
    int getState();

private:
    int mState;
};
#endif // !defined(EA_8E02386E_3A21_48f6_9655_702E0977DB5E__INCLUDED_)
