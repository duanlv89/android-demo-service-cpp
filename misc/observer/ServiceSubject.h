///////////////////////////////////////////////////////////
//  ServiceSubject.h
//  Implementation of the Class ServiceSubject
//  Created on:      06-Apr-2020 12:47:43 PM
//  Original author: duanlv
///////////////////////////////////////////////////////////

#if !defined(EA_F1F4B647_5304_41d5_905D_3DBF3FF309A9__INCLUDED_)
#define EA_F1F4B647_5304_41d5_905D_3DBF3FF309A9__INCLUDED_

#include "ClientObserver.h"
#include "Subject.h"

class ServiceSubject : public Subject {

public:
    ServiceSubject();
    virtual ~ServiceSubject();

    void registerObserver(Observer* observer);
    void unregisterObserver(Observer* observer);
    void notify();
    int getState();
    void setState(int state);

private:
    std::vector<Observer*> observers;
    int mState;
};

struct ObserverCmp {
    explicit ObserverCmp(uint16_t id)
        : id(id)
    {
    }
    inline bool operator()(const Observer* p) const { return (p->myId == id); }
    uint16_t id;
};

#endif // !defined(EA_F1F4B647_5304_41d5_905D_3DBF3FF309A9__INCLUDED_)
