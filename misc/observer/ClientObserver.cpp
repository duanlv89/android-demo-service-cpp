///////////////////////////////////////////////////////////
//  ClientObserver.cpp
//  Implementation of the Class ClientObserver
//  Created on:      06-Apr-2020 12:47:53 PM
//  Original author: duanlv
///////////////////////////////////////////////////////////

#include "ClientObserver.h"
#include "Subject.h"
#include <stdint.h>

ClientObserver::ClientObserver(uint16_t id = 0)
    : Observer(id)
    , mState(-1)
{
}

ClientObserver::~ClientObserver()
{
}

void ClientObserver::onNotification(Subject* subject)
{
    mState = subject->getState();
}

int ClientObserver::getState()
{
    return mState;
}
