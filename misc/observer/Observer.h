///////////////////////////////////////////////////////////
//  Observer.h
//  Implementation of the Interface Observer
//  Created on:      06-Apr-2020 12:37:58 PM
//  Original author: duanlv
///////////////////////////////////////////////////////////

#if !defined(EA_BE0E96FC_F837_492f_9D7E_86A2F1CCB4D0__INCLUDED_)
#define EA_BE0E96FC_F837_492f_9D7E_86A2F1CCB4D0__INCLUDED_

#include <stdint.h>

class Subject;

class Observer {

public:
    Observer(int id)
        : myId(id)
    {
    }
    virtual ~Observer() {}
    virtual void onNotification(Subject* subject) = 0;

public:
    uint16_t myId;
};
#endif // !defined(EA_BE0E96FC_F837_492f_9D7E_86A2F1CCB4D0__INCLUDED_)
