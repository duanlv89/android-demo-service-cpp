///////////////////////////////////////////////////////////
//  ServiceSubject.cpp
//  Implementation of the Class ServiceSubject
//  Created on:      06-Apr-2020 12:47:43 PM
//  Original author: duanlv
///////////////////////////////////////////////////////////

#include "ServiceSubject.h"
#include "Observer.h"
#include <algorithm>

ServiceSubject::ServiceSubject()
    : mState(-1)
{
}

ServiceSubject::~ServiceSubject()
{
}

void ServiceSubject::notify()
{
    for (size_t i = 0; i < observers.size(); i++) {
        observers.at(i)->onNotification(this);
    }
}

int ServiceSubject::getState()
{
    return mState;
}

void ServiceSubject::setState(int state)
{
    mState = state;
}

void ServiceSubject::registerObserver(Observer* observer)
{
    if (observer != nullptr)
        observers.push_back(observer);
}

void ServiceSubject::unregisterObserver(Observer* observer)
{
    std::vector<Observer*>::iterator it;
    it = std::find_if(observers.begin(), observers.end(),
        ObserverCmp(observer->myId));
    if (it != observers.end()) {
        observers.erase(it);
    }
}
