#include <utils/Log.h>  
#include "DemoService/IDemoService.h"

using namespace android;

class BpDemoService : public BpInterface<IDemoService>
{
public:
    BpDemoService(const sp<IBinder>& impl) : BpInterface<IDemoService>(impl){};
    int addNumber(int a, int b)
    {
        ALOGD("BpDemoService AddNumber a:%d , b:%d\n", a, b);

        Parcel data;  
        data.writeInterfaceToken(IDemoService::getInterfaceDescriptor());  
        data.writeInt32(a);
        data.writeInt32(b);

        Parcel reply;  
        remote()->transact(ADD_NUMBER, data, &reply);
    
        int32_t exception = reply.readExceptionCode();
        return reply.readInt32();
    }

    int maxNumber(int a, int b)
    {
        ALOGD("BpDemoService MaxNumber a:%d , b:%d\n", a, b);
        Parcel data;  
        data.writeInterfaceToken(IDemoService::getInterfaceDescriptor());  
        data.writeInt32(a);
        data.writeInt32(b);
        
        Parcel reply;  
        remote()->transact(MAX_NUMBER, data, &reply);

        int32_t exception = reply.readExceptionCode();
        return reply.readInt32();
    }

    void addNumberAsync(int a, int b)
    {
        ALOGD("BpDemoService AddNumber a:%d , b:%d\n", a, b);

        Parcel data;  
        data.writeInterfaceToken(IDemoService::getInterfaceDescriptor());  
        data.writeInt32(a);
        data.writeInt32(b);
        remote()->transact(ADD_NUMBER_ASYNC, data, nullptr);
    }

    void maxNumberAsync(int a, int b)
    {
        ALOGD("BpDemoService MaxNumber a:%d , b:%d\n", a, b);
        Parcel data;  
        data.writeInterfaceToken(IDemoService::getInterfaceDescriptor());  
        data.writeInt32(a);
        data.writeInt32(b);
        remote()->transact(MAX_NUMBER_ASYNC, data, nullptr);
    }

    int registerListener(const sp<IDemoListener>& listener)
    {
        ALOGD("registerListener");
        Parcel data;
        data.writeInterfaceToken(IDemoService::getInterfaceDescriptor());  
        data.writeStrongBinder(IInterface::asBinder(listener));
        remote()->transact(REG_LISTENER, data, nullptr);

        return 0;
    }
};

IMPLEMENT_META_INTERFACE(DemoService, "platform.demoservice.IDemoService");

status_t BnDemoService::onTransact(uint32_t code,const Parcel & data,Parcel * reply, uint32_t flags)
{
    switch(code)
    {
        case ADD_NUMBER:
        {
            CHECK_INTERFACE(IDemoService, data, reply);
            int ret = addNumber(data.readInt32(), data.readInt32());
            // ALOGD("BpDemoService ADD_NUMBER ret:%d\n", ret);
            reply->writeNoException();
            reply->writeInt32(ret);
            return NO_ERROR;
        }
        case MAX_NUMBER:
        {
            CHECK_INTERFACE(IDemoService, data, reply);
            int ret = maxNumber(data.readInt32(), data.readInt32());
            // ALOGD("BpDemoService MAX_NUMBER ret:%d\n", ret);
            reply->writeNoException();
            reply->writeInt32(ret);
            return NO_ERROR;
        }
        // Asynchronous calls
        case ADD_NUMBER_ASYNC:
        {
            CHECK_INTERFACE(IDemoService, data, reply);
            addNumberAsync(data.readInt32(), data.readInt32());
            return NO_ERROR;
        }
        case MAX_NUMBER_ASYNC:
        {
            CHECK_INTERFACE(IDemoService, data, reply);
            maxNumberAsync(data.readInt32(), data.readInt32());
            return NO_ERROR;
        }
        case REG_LISTENER:
        {
            CHECK_INTERFACE(IDemoService, data, reply);
            sp<IDemoListener> listener = interface_cast<IDemoListener>(data.readStrongBinder());
            registerListener(listener);
        }
        default:
            return BBinder::onTransact(code, data, reply, flags);
    }
}
