#include "DemoService.h"
#include <binder/IPCThreadState.h>
#include <binder/IServiceManager.h>

#include <utils/Log.h>

DemoService::DemoService()
{
    //@TODO: ctor
}

void DemoService::instance(void)
{
    defaultServiceManager()->addService(String16(DEMO_SERVICE_NAME), new DemoService::DemoServiceStub(*this));
}

DemoService::~DemoService()
{
    //@TODO: dtor
}

int DemoService::addNumber(int a,int b)
{
    // ALOGD("DemoService AddNumber a:%d, b:%d\n", a, b);
    return a + b;
}

int DemoService::maxNumber(int a, int b)
{
    // ALOGD("DemoService MaxNumber a:%d, b:%d\n", a, b);
    return a > b ? a : b;
}

void DemoService::addNumberAsync(int a, int b)
{
    int32_t result = addNumber(a, b);
    // mpListener->onAddNumberRlt(result);
    notify(result);
}

void DemoService::notify(int32_t result)
{
    for (size_t i = 0; i < listeners.size(); i++) {
        listeners.at(i)->onAddNumberRlt(result);
    }
}

void DemoService::maxNumberAsync(int a, int b)
{
    int32_t result = maxNumber(a, b);
    // mpListener->onMaxNumberRlt(result);
}

int DemoService::registerListener(const sp<IDemoListener>& listener)
{
    if (listener != nullptr)
        listeners.push_back(listener);
}

int DemoService::DemoServiceStub::addNumber(int a, int b)
{
    return mParent.addNumber(a, b);
}

int DemoService::DemoServiceStub::maxNumber(int a, int b)
{
    return mParent.maxNumber(a, b);
}

void DemoService::DemoServiceStub::addNumberAsync(int a, int b)
{
    mParent.addNumberAsync(a, b);
}

void DemoService::DemoServiceStub::maxNumberAsync(int a, int b)
{
    mParent.maxNumberAsync(a, b);
}

int DemoService::DemoServiceStub::registerListener(const sp<IDemoListener>& listener)
{
    return mParent.registerListener(listener);
}
