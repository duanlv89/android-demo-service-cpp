#include <utils/Log.h>  
#include "DemoService/IDemoListener.h"

using namespace android;

class BpDemoListener : public BpInterface<IDemoListener> {
public:
    BpDemoListener(const sp<IBinder>& impl) : BpInterface<IDemoListener>(impl){};
    void onAddNumberRlt(int32_t result)
    {
        Parcel data;
        data.writeInterfaceToken(IDemoListener::getInterfaceDescriptor());  
        data.writeInt32(result);
        remote()->transact(TRANSACT_ON_ADDRESULT, data, nullptr);
    }

    void onMaxNumberRlt(int32_t result)
    {
        Parcel data;
        data.writeInterfaceToken(IDemoListener::getInterfaceDescriptor());  
        data.writeInt32(result);
        remote()->transact(TRANSACT_ON_MAXRESULT, data, nullptr);
    }
};

IMPLEMENT_META_INTERFACE(DemoListener, "platform.demoservice.IDemoListener");

status_t BnDemoListener::onTransact(uint32_t code,const Parcel & data,Parcel * reply, uint32_t flags)
{
    switch(code) {
        case TRANSACT_ON_ADDRESULT:
        {
            CHECK_INTERFACE(IDemoListener, data, reply);
            int result = data.readInt32();
            onAddNumberRlt(result);
            return NO_ERROR;
        }
        case TRANSACT_ON_MAXRESULT:
        {
            CHECK_INTERFACE(IDemoListener, data, reply);
            int result = data.readInt32();
            onMaxNumberRlt(result);
            return NO_ERROR;
        }
        default:
            return BBinder::onTransact(code, data, reply, flags);
    }
}