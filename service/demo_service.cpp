#include <utils/Log.h>
#include <binder/IServiceManager.h>
#include <binder/IPCThreadState.h>
#include "DemoService/IDemoService.h"
#include "DemoService.h"

int main(int argc, char** argv)
{
    DemoService* pDemoSvc = new DemoService();

    pDemoSvc->instance();

    ProcessState::self()->startThreadPool();
    IPCThreadState::self()->joinThreadPool();

    return 0;
}
