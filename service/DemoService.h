#ifndef __DEMO_SERVICE_H__
#define __DEMO_SERVICE_H__

#include <vector>
#include "DemoService/IDemoListener.h"
#include "DemoService/IDemoService.h"

using namespace android;

class DemoService;

class DemoService : public RefBase {
public:
    DemoService();
    virtual ~DemoService();
    void instance(void);

public:
    class DemoServiceStub : public BnDemoService {
    public:
        DemoServiceStub(DemoService& parent)
            : mParent(parent)
        {
        }
        virtual ~DemoServiceStub() {}
        virtual int addNumber(int a, int b);
        virtual int maxNumber(int a, int b);
        virtual void addNumberAsync(int a, int b);
        virtual void maxNumberAsync(int a, int b);
        virtual int registerListener(const sp<IDemoListener>& listener);

    private:
        DemoService& mParent;
        friend class DemoService;
    };

private:
    int addNumber(int a, int b);
    int maxNumber(int a, int b);
    void addNumberAsync(int a, int b);
    void maxNumberAsync(int a, int b);
    int registerListener(const sp<IDemoListener>& listener);
    void notify(int32_t result);

private:
    std::vector<sp<IDemoListener>> listeners;
};

#endif /* __DEMO_SERVICE_H__ */
