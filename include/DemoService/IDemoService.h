#ifndef __I_DEMO_MANAGER_SERVICE_H__
#define __I_DEMO_MANAGER_SERVICE_H__

#include "DemoService/IDemoListener.h"
#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Parcel.h>
#include <utils/Errors.h>
#include <utils/RefBase.h>

using namespace android;

#define DEMO_SERVICE_NAME "platform.demoservice"

enum {
    ADD_NUMBER = IBinder::FIRST_CALL_TRANSACTION,
    MAX_NUMBER,
    ADD_NUMBER_ASYNC,
    MAX_NUMBER_ASYNC,
    REG_LISTENER
};

class IDemoService : public IInterface {
public:
    DECLARE_META_INTERFACE(DemoService);

    virtual int addNumber(int a, int b) = 0;
    virtual int maxNumber(int a, int b) = 0;
    virtual void addNumberAsync(int a, int b) = 0;
    virtual void maxNumberAsync(int a, int b) = 0;
    virtual int registerListener(const sp<IDemoListener>& listener) = 0;
};

class BnDemoService : public BnInterface<IDemoService> {
public:
    virtual status_t onTransact(uint32_t code, const Parcel& data, Parcel* reply,
        uint32_t flags = 0);
};

#endif /* __I_DEMO_MANAGER_SERVICE_H__ */
