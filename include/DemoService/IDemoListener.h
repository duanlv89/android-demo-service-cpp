#if !defined(_I_DEMO_LISTENER_H_)
#define _I_DEMO_LISTENER_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Parcel.h>
#include <utils/Errors.h>
#include <utils/RefBase.h>

using namespace android;

enum {
    TRANSACT_ON_ADDRESULT = IBinder::FIRST_CALL_TRANSACTION,
    TRANSACT_ON_MAXRESULT
};

class IDemoListener : public IInterface {
public:
    DECLARE_META_INTERFACE(DemoListener);
    virtual void onAddNumberRlt(int32_t result) = 0;
    virtual void onMaxNumberRlt(int32_t result) = 0;

public:
    uint16_t myId;
};

class BnDemoListener : public BnInterface<IDemoListener> {
public:
    virtual status_t onTransact(uint32_t code, const Parcel& data, Parcel* reply,
        uint32_t flags = 0);
};

#endif //! defined(_I_DEMO_LISTENER_H_)
